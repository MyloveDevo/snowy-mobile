const constant = {
	envKey: "envKey",
	allEnv: "allEnv",
	sysBaseConfig: 'sysBaseConfig',
	homeConfigs: "homeConfigs",
	userInfo: 'userInfo',
	userMenus: 'userMenus',
	userMobileMenus: 'userMobileMenus',
	dictTypeTreeData: 'dictTypeTreeData',
}
export default constant